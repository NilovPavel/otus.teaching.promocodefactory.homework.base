﻿namespace Models.Requests
{
    /// <summary>
    /// Запрос на создание пользователя
    /// </summary>
    public class CreationEmployeeRequest
    {
        /// <summary>
        /// E-mail пользователя
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string LastName { get; set; }

    }
}
