﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Requests
{
    /// <summary>
    /// Запрос по обновлению данных работника
    /// </summary>
    public class UpdateEmployeeRequest
    {
        /// <summary>
        /// Guid работника
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// E-mail пользователя
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string? LastName { get; set; }

        /// <summary>
        /// Идентификатор новой роли пользователя
        /// </summary>
        public string? RoleId { get; set; }

        /// <summary>
        /// Промокоды пользователя
        /// </summary>
        public int? AppliedPromocodesCount { get; set; }
    }
}
