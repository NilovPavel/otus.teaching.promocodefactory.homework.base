﻿using System;

namespace Models.Response
{
    /// <summary>
    /// Класс роли
    /// </summary>
    public class RoleItemResponse
    {
        /// <summary>
        /// Идентификатор роли в хранилище
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Наименование роли
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Описание роли
        /// </summary>
        public string Description { get; set; }
    }
}