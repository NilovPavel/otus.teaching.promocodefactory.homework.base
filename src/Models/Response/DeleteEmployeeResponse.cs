﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Response
{
    /// <summary>
    /// Ответ при удалении пользователя
    /// </summary>
    public class DeleteEmployeeResponse
    {
        /// <summary>
        /// Результат выполнения операции
        /// </summary>
        public bool Success { get; set; }
    }
}
