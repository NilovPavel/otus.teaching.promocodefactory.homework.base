﻿using Models.Response;
using System;
using System.Collections.Generic;

namespace Models.Response
{
    /// <summary>
    /// Ответ по созданию/обновлению записи о работнике
    /// </summary>
    public class EmployeeResponse
    {
        /// <summary>
        /// Id работника
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Полное имя работника
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Email работника
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Роли работника
        /// </summary>
        public List<RoleItemResponse> Roles { get; set; }
        /// <summary>
        /// Счетчик промокодов какой-то 
        /// </summary>
        public int AppliedPromocodesCount { get; set; }
    }
}