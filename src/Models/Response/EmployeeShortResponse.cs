﻿using System;

namespace Models.Response
{
    /// <summary>
    /// Ответ по краткой информации о работнике
    /// </summary>
    public class EmployeeShortResponse
    {
        /// <summary>
        /// Id работника
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Полное имя работника
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Email работника
        /// </summary>
        public string Email { get; set; }
    }
}