﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models;
using Models.Requests;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<T> AddEmployee(CreationEmployeeRequest employee);
        Task<bool> DeleteEmployee(string id);
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);
        Task<T> UpdateEmployee(UpdateEmployeeRequest updateEmployeeRequest);
    }
}