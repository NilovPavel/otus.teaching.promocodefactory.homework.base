﻿using Microsoft.AspNetCore.Mvc;
using Models.Requests;
using Models.Response;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Внести сотрудника
        /// </summary>
        /// <returns>Возвращает </returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> AddEmployeeAsync(CreationEmployeeRequest employee)
        {
            Employee empl = 
                await _employeeRepository.AddEmployee(employee);

            EmployeeResponse result = new EmployeeResponse
            {
                Id = empl.Id,
                FullName = empl.FullName,
                Email = empl.Email,
                Roles = empl.Roles.Select(item => new RoleItemResponse { Id = item.Id, Name = item.Name, Description = item.Description }).ToList(),
                AppliedPromocodesCount = empl.AppliedPromocodesCount
            };

            return result;
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns>Возвращает </returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<bool>> DeleteEmployeeAsync(string id)
        {
            bool result =
                await _employeeRepository.DeleteEmployee(id);

            DeleteEmployeeResponse deleteEmployeeResponse = new DeleteEmployeeResponse
            {
                Success = result
            };

            return result;
        }

        /// <summary>
        /// Обновить сотрудника
        /// </summary>
        /// <returns>Новая структура данных сотрудника</returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync(UpdateEmployeeRequest updateEmployeeRequest)
        {
            Employee result =
                await _employeeRepository.UpdateEmployee(updateEmployeeRequest);

            if (result==null) 
                return NotFound();

            EmployeeResponse updateEmployeeResponse = new EmployeeResponse
            {
                Id = result.Id,
                Email = result.Email,
                FullName = result.FullName,
                Roles = result.Roles.Select(item => new RoleItemResponse { Id = item.Id, Name = item.Name, Description = item.Description }).ToList(),
                AppliedPromocodesCount = result.AppliedPromocodesCount
            };

            return updateEmployeeResponse;
        }
    }
}