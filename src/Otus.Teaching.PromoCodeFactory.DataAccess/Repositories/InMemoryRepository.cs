﻿using Models;
using Models.Requests;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddEmployee(CreationEmployeeRequest employeeRequest)
        {
            Employee _employee = new Employee 
            {
                Id = Guid.NewGuid(),
                Email = employeeRequest.Email,
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Roles = new List<Role>()
                {
                    FakeDataFactory.Roles.FirstOrDefault(x => x.Name == "PartnerManager")
                },
                AppliedPromocodesCount = 10
            };
            _employee.Id = Guid.NewGuid();
            ((List<T>)this.Data).Add((T)(BaseEntity)_employee);
            return Task.FromResult((T)(BaseEntity)_employee);
        }

        public Task<bool> DeleteEmployee(string id)
        {
            T employeeById = 
                (T)(BaseEntity)this.Data.FirstOrDefault(x => x.Id.ToString().Equals(id));

            bool isOk = ((List<T>)this.Data).Remove(employeeById);
            
            return Task.FromResult(isOk);
        }

        public Task<T> UpdateEmployee(UpdateEmployeeRequest updateEmployeeRequest)
        {
            Employee currentEmployee = (Employee)(BaseEntity)this.Data.FirstOrDefault(x => x.Id.ToString().Equals(updateEmployeeRequest.Id));
            Role currentRole = FakeDataFactory.Roles.FirstOrDefault(x => x.Id.ToString().Equals((string)updateEmployeeRequest.RoleId));

            if (currentEmployee == null) 
                return Task.FromResult((T)(BaseEntity)default(Employee));

            currentEmployee.FirstName = updateEmployeeRequest.FirstName ?? currentEmployee.FirstName;
            currentEmployee.LastName = updateEmployeeRequest.LastName ?? currentEmployee.LastName;
            currentEmployee.Email = updateEmployeeRequest.Email ?? currentEmployee.Email;
            currentEmployee.AppliedPromocodesCount = updateEmployeeRequest.AppliedPromocodesCount ?? 0;
            
            if(!currentEmployee.Roles.Contains(currentRole) && currentRole!=null)
                currentEmployee.Roles.Add(currentRole);

            return Task.FromResult((T)(BaseEntity)currentEmployee);
        }
    }
}